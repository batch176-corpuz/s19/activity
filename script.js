// #3 to #4
let num = 2;
const getCube = num ** 3;

console.log(`The cube of ${num} is ${getCube}.`);

// #5 to #6
let address = ["258 Washington Ave", "NW, California", 90011];
const [street, state, zip] = address;

console.log(`I live at ${street} ${state} ${zip}`);

// #7 to #8
const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	height: "20 ft 3 in",
};
const { name, type, weight, height } = animal;

console.log(
	`${name} was a ${type}. He weighed at ${weight} kgs with a measurement of ${height}.`
);

// #9 to #10
const numbersArr = [1, 2, 3, 4, 5];
numbersArr.forEach((number) => console.log(number));

// #11 to #12
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const baby = new Dog("Baby", 3, "Handsome Street Dog");
console.log(baby);
